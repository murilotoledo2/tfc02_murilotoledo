package com.example.tfc02


import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter

class ChatAdapter:  Adapter<RecyclerView.ViewHolder>(){

    private val items: MutableList<String> = mutableListOf()

    fun addItem(message: String){
        items.add(message)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val card = LayoutInflater.from(parent.context).inflate(R.layout.chat_card, parent, false)
        return MessageViewHolder(card)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val currentItem = items[position]
        if (holder is MessageViewHolder){
            holder.tituloTextView.text = currentItem
            holder.descTextView.text = currentItem

            holder.itemView.setOnClickListener(View.OnClickListener {
                Log.d("Clique ","Curto no " + items[position])

                //val intent = Intent(this,MessageActivity::class.java)


            })
        }

    }


    override fun getItemCount(): Int {
        return items.size
    }

    class MessageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val imgChatView: ImageView = itemView.findViewById(R.id.imgChat)
        val tituloTextView: TextView = itemView.findViewById(R.id.titulo_txtview)
        val descTextView: TextView = itemView.findViewById(R.id.descricao_txtview)
    }

}