package com.example.tfc02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import model.ChatChannel


class MainActivity : AppCompatActivity() {

    //val listaCanais: ArrayList<ChatChannel>()
    var contador: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpListeners()
        setUpRecyclerView()

    }

    private fun setUpListeners(){
        criarCanal_btn.setOnClickListener{
            val adapter = listaCanal.adapter

            if(adapter is ChatAdapter){
                contador = contador+1
                adapter.addItem(message = "Chat "+contador)
                listaCanal.scrollToPosition(adapter.itemCount-1)

            }

        }
    }

    private fun setUpRecyclerView(){
        listaCanal.layoutManager = LinearLayoutManager(this)
        listaCanal.adapter = ChatAdapter()
    }
}